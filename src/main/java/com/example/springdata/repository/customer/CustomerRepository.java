package com.example.springdata.repository.customer;

import com.example.springdata.repository.customer.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long> {
}