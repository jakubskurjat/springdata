package com.example.springdata.repository.order.entity;

import com.example.springdata.repository.customer.entity.Customer;
import com.example.springdata.repository.product.entity.Product;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Customer customer;

    @ManyToMany(cascade = CascadeType.MERGE)
    private Set<Product> products;

    private LocalDateTime placeDate;

    private String status;

    public Order(@JsonProperty("customer") Customer customer, @JsonProperty("products") Set<Product> products,
                 @JsonProperty("placeData") LocalDateTime placeDate, @JsonProperty("status") String status) {
        this.customer = customer;
        this.products = products;
        this.placeDate = placeDate;
        this.status = status;
    }
}
