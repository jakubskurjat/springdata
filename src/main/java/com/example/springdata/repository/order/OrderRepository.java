package com.example.springdata.repository.order;

import com.example.springdata.repository.order.entity.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}