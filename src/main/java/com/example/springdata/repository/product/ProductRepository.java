package com.example.springdata.repository.product;

import com.example.springdata.repository.product.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}