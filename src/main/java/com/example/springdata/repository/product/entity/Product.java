package com.example.springdata.repository.product.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private float price;

    private boolean available;

    public Product(@JsonProperty("productName") String name, @JsonProperty("price") float price,
                   @JsonProperty("available") boolean available) {
        this.name = name;
        this.price = price;
        this.available = available;
    }
}