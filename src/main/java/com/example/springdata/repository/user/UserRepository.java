package com.example.springdata.repository.user;

import com.example.springdata.repository.user.entity.UserDto;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserDto, Long> {
}