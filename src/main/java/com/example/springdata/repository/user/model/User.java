package com.example.springdata.repository.user.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    private String name;
    private String password;
    private String role;
}
