package com.example.springdata;

import com.example.springdata.repository.customer.CustomerRepository;
import com.example.springdata.repository.customer.entity.Customer;
import com.example.springdata.repository.order.OrderRepository;
import com.example.springdata.repository.order.entity.Order;
import com.example.springdata.repository.product.ProductRepository;
import com.example.springdata.repository.product.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepository productRepository;
    private OrderRepository orderRepository;
    private CustomerRepository customerRepository;

    @Autowired
    public DbMockData(ProductRepository productRepository, OrderRepository orderRepository, CustomerRepository customerRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {

        Product product1 = new Product("Basketball shoes", 500f, true);
        Product product2 = new Product("Sleeve", 50f, true);
        Product product3 = new Product("Basketball shorts", 150f, true);
        Product product4 = new Product("Ball", 200f, true);
        Product product5 = new Product("Socks", 30f, true);
        Customer customer1 = new Customer("Jan Kowalski", "Kraków");
        Customer customer2 = new Customer("Andrzej Nowak", "Warszawa");

        Set<Product> productSet1 = new HashSet<>() {
            {
                add(product1);
                add(product3);
                add(product5);
            }
        };

        Set<Product> productSet2 = new HashSet<>() {
            {
                add(product1);
                add(product2);
                add(product3);
                add(product4);
                add(product5);
            }
        };

        Order order1 = new Order(customer1, productSet1, LocalDateTime.parse(String.valueOf(LocalDateTime.now()).substring(0, 16)), "in the hooper");
        Order order2 = new Order(customer2, productSet2, LocalDateTime.parse("2021-04-19T10:15"), "in the hooper");

        productRepository.save(product1);
        productRepository.save(product2);
        productRepository.save(product3);
        productRepository.save(product4);
        productRepository.save(product5);

        customerRepository.save(customer1);
        customerRepository.save(customer2);

        orderRepository.save(order1);
        orderRepository.save(order2);
    }
}
