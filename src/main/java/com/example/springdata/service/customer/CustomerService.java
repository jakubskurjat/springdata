package com.example.springdata.service.customer;

import com.example.springdata.repository.customer.CustomerRepository;
import com.example.springdata.repository.customer.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Optional<Customer> getCustomerById(long id) {
        return customerRepository.findById(id);
    }

    public Iterable<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Customer addCustomer(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer updateCustomer(Customer customer, long id) {
        Optional<Customer> customerFromDB = customerRepository.findById(id);
        customerFromDB.get().setAddress(customer.getAddress());
        customerFromDB.get().setName(customer.getName());

        return customerRepository.save(customerFromDB.get());
    }

    public void updatePartialCustomer(Map<String, Object> updates, long id) {
        Optional<Customer> customerFromDB = customerRepository.findById(id);

        partialUpdates(customerFromDB.get(), updates);
    }

    private void partialUpdates(Customer customer, Map<String, Object> updates) {
        if (updates.containsKey("customerName")) customer.setName((String) updates.get("customerName"));
        if (updates.containsKey("address")) customer.setAddress((String) updates.get("address"));

        customerRepository.save(customer);
    }
}