package com.example.springdata.service.product;

import com.example.springdata.repository.product.ProductRepository;
import com.example.springdata.repository.product.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Optional<Product> getProductById(long id) {
        return productRepository.findById(id);
    }

    public Iterable<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Product updateProduct(Product product, long id) {
        Optional<Product> productFromDB = productRepository.findById(id);
        productFromDB.get().setName(product.getName());
        productFromDB.get().setPrice(product.getPrice());
        productFromDB.get().setAvailable(product.isAvailable());

        return productRepository.save(productFromDB.get());
    }

    public void updatePartialProduct(Map<String, Object> updates, long id) {
        Optional<Product> productFromDB = productRepository.findById(id);

        partialUpdates(productFromDB.get(), updates);
    }

    private void partialUpdates(Product product, Map<String, Object> updates) {
        if (updates.containsKey("productName")) product.setName((String) updates.get("productName"));
        if (updates.containsKey("address")) product.setPrice((Float) updates.get("price"));
        if (updates.containsKey("available")) product.setAvailable((Boolean) updates.get("available"));

        productRepository.save(product);
    }
}
