package com.example.springdata.service.user;

import com.example.springdata.repository.user.UserRepository;
import com.example.springdata.repository.user.builder.UserDtoBuilder;
import com.example.springdata.repository.user.configuration.PasswordEncoderConfig;
import com.example.springdata.repository.user.entity.UserDto;
import com.example.springdata.repository.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;
    PasswordEncoderConfig passwordEncoderConfig;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoderConfig passwordEncoderConfig) {
        this.userRepository = userRepository;
        this.passwordEncoderConfig = passwordEncoderConfig;
    }

    private Optional<UserDto> getUser(long id) {
        return userRepository.findById(id);
    }

    private void addUser(UserDto user) {
        userRepository.save(user);
    }

    public UserDto validate(User user) {
        Iterable<UserDto> userList = userRepository.findAll();

        for (UserDto user1 : userList) {
            if (user1.getName().equals(user.getName()) && passwordEncoderConfig.passwordEncoder().matches(user.getPassword(), user1.getPasswordHash())) {
                return new UserDto(user1.getName(), user1.getPasswordHash(), user1.getRole());
            }
        }

        return null;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        if (getUser(1L).isEmpty()) {
            User user1 = new User("KuBa", "KuBa21", "ROLE_ADMIN");
            User user2 = new User("Janusz", "Janusz06", "ROLE_CUSTOMER");

            UserDto userDto1 = UserDtoBuilder.addNewUser(user1);
            UserDto userDto2 = UserDtoBuilder.addNewUser(user2);

            addUser(userDto1);
            addUser(userDto2);
        }
    }
}
