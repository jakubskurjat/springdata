package com.example.springdata;

import com.example.springdata.token.JWTFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    public WebSecurityConfig() {
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(HttpMethod.POST, "/api/getToken");
        web.ignoring().antMatchers(HttpMethod.GET, "/api/product/*");
        web.ignoring().antMatchers(HttpMethod.GET, "/api/order/*");
        web.ignoring().antMatchers(HttpMethod.POST, "/api/order");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()

                .antMatchers("/api/customer/*").hasRole("CUSTOMER")
                .antMatchers(HttpMethod.POST, "/api/admin/product").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/product/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/product/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/api/admin/customer").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/customer/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/customer/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/api/admin/order/{id}").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/admin/order/{id}").hasRole("ADMIN")
                .and().httpBasic()
                .and().csrf().disable()
                .addFilter(new JWTFilter(authenticationManager()));
    }

}
