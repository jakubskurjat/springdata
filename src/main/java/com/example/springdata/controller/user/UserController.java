package com.example.springdata.controller.user;

import com.example.springdata.repository.user.entity.UserDto;
import com.example.springdata.repository.user.model.User;
import com.example.springdata.service.user.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@RestController
@RequestMapping("/api")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/getToken")
    public String getToken(@RequestBody User user) {
        long currentTimeMillis = System.currentTimeMillis();

        UserDto u = userService.validate(user);

        if (u != null) {

            return Jwts.builder()
                    .claim("name", u.getName())
                    .claim("role", u.getRole())
                    .setIssuedAt(new Date(currentTimeMillis))
                    .setExpiration(new Date(currentTimeMillis + 20000))
                    .signWith(SignatureAlgorithm.HS512, "pass".getBytes())
                    .compact();
        }

        return "Validation error";
    }
}