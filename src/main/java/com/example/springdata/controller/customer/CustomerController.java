package com.example.springdata.controller.customer;

import com.example.springdata.repository.customer.entity.Customer;
import com.example.springdata.service.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CustomerController {

    private CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/customer/{id}")
    public Optional<Customer> getCustomerById(@PathVariable long id) {
        return customerService.getCustomerById(id);
    }

    @GetMapping("/customer/all")
    public Iterable<Customer> getAllCustomers() {
        return customerService.getAllCustomers();
    }

    @PostMapping("/admin/customer")
    public Customer addCustomer(@RequestBody Customer customer) {
        return customerService.addCustomer(customer);
    }

    @PutMapping("/admin/customer/{id}")
    public Customer updateCustomer(@RequestBody Customer customer, @PathVariable long id) {
        return customerService.updateCustomer(customer, id);
    }

    @PatchMapping("/admin/customer/{id}")
    public void updatePartialCustomer(@RequestBody Map<String, Object> updates, @PathVariable long id) {
        customerService.updatePartialCustomer(updates, id);
    }
}