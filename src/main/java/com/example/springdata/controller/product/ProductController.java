package com.example.springdata.controller.product;

import com.example.springdata.repository.product.entity.Product;
import com.example.springdata.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductController {

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product/{id}")
    public Optional<Product> getProductById(@PathVariable long id) {
        return productService.getProductById(id);
    }

    @GetMapping("/product/all")
    public Iterable<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @PostMapping("/admin/product")
    public Product addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @PutMapping("/admin/product/{id}")
    public Product updateProduct(@RequestBody Product product, @PathVariable long id) {
        return productService.updateProduct(product, id);
    }

    @PatchMapping("/admin/product/{id}")
    public void updatePartialProduct(@RequestBody Map<String, Object> updates, @PathVariable long id) {
        productService.updatePartialProduct(updates, id);
    }
}