package com.example.springdata.controller.order;

import com.example.springdata.repository.order.entity.Order;
import com.example.springdata.service.order.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class OrderController {

    private OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/order/{id}")
    public Optional<Order> getOrderById(@PathVariable long id) {
        return orderService.getOrderById(id);
    }

    @GetMapping("/order/all")
    public Iterable<Order> getAllOrders() {
        return orderService.getAllOrders();
    }

    @PostMapping("/order")
    public ResponseEntity addOrder(@RequestBody Map<String, Object> elementsOfOrders) {
        return orderService.addOrder(elementsOfOrders);
    }

    @PutMapping("/admin/order/{id}")
    public ResponseEntity updateOrder(@RequestBody Map<String, Object> updates, @PathVariable long id) {
        return orderService.updateAllOrPartialOrder(updates, id);
    }

    @PatchMapping("/admin/order/{id}")
    public ResponseEntity updatePartialOrder(@RequestBody Map<String, Object> updates, @PathVariable long id) {
        return orderService.updateAllOrPartialOrder(updates, id);
    }
}